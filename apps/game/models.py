
from django.db import models
from django.core.validators import MinValueValidator


class Game(models.Model):

    GAME_QUESTIONS_COUNT = 6

    reached_score = models.IntegerField(validators=[MinValueValidator(0)], default=0)
    is_won = models.BooleanField(default=False)
    answered_questions = models.CharField(max_length=255)

    def __str__(self):
        return 'Game ID: {}'.format(self.pk)
