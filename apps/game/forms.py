
from django import forms

from .mixins import GameMixin
from apps.game_components.models import QuestionAnswer


class GameProcessForm(forms.Form, GameMixin):

    answer = forms.ModelChoiceField(queryset=QuestionAnswer.objects.all())

    def __init__(self, *args, **kwargs):

        current_question = kwargs.pop('request').session['game']['current_question']
        super().__init__(*args, **kwargs)

        # Allow the get answers of the current question only.
        self.fields['answer'].queryset = QuestionAnswer.objects.filter(question_id=current_question)
