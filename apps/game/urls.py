
from django.urls import path

from . import views

app_name = 'game'

urlpatterns = [
    path('start/', views.StartGame.as_view(), name='start'),
    path('process/', views.GameProcess.as_view(), name='process'),
]
