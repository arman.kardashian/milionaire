
from django.shortcuts import redirect
from django.views.generic import TemplateView

from .models import Game
from .mixins import GameMixin
from .forms import GameProcessForm
from apps.game_components.models import Question


class StartGame(TemplateView):

    template_name = 'game/start.html'

    def post(self, request, *args, **kwargs):

        self.__init_session(request)
        return redirect('game:process')

    def __init_session(self, request):

        request.session['game'] = {
            'current_question': Question.objects.order_by('?').first().id,
            'past_questions': list(),
            'reached_score': 0,
        }


class GameProcess(TemplateView, GameMixin):

    template_name = 'game/process.html'
    form_class = GameProcessForm

    def get(self, request, *args, **kwargs):

        self._validate_session(request)
        return self._render_next_question(request)

    def post(self, request, *args, **kwargs):

        self._validate_session(request)

        # Get the answer from the Form.
        form = self.form_class(data=request.POST, request=request)
        if not form.is_valid():
            raise Exception(form.errors)
        answer = form.cleaned_data['answer']

        # If the answer is NOT right, then count the current score and end the game.
        if not answer.is_right:
            return self._render_end_of_game(request, is_won=False)

        # Update game details
        request.session['game']['past_questions'].append(answer.question_id)
        request.session['game']['reached_score'] += answer.question.score

        # If current answer is equal to GAME_QUESTIONS_COUNT, then finalize the game.
        if len(request.session['game']['past_questions']) == Game.GAME_QUESTIONS_COUNT:
            return self._render_end_of_game(request, is_won=True)

        # If answer is right and it is possible to continue the game, then get the next question (unique for the game).
        next_question = Question.objects.exclude(id__in=request.session['game']['past_questions']).order_by('?').first()
        request.session['game']['current_question'] = next_question.id

        return self._render_next_question(request)
