
from django.shortcuts import render

from apps.game.models import Game
from apps.game_components.models import Question


class GameMixin(object):

    template_name = None

    def _render_end_of_game(self, request, is_won):
        """
        Store the game details and finalize it.
        Generate the game results data and render the response.
        :param request: WSGIRequest object
        :param is_won: Boolean
        :return: HttpResponse
        """

        # Store Game information
        Game.objects.create(
            is_won=is_won,
            reached_score=request.session['game']['reached_score'],
            answered_questions=str(request.session['game']['past_questions'])
        )

        response = {
            'reached_score': request.session['game']['reached_score'],
            'game_status': is_won,
            'is_finished': True
        }

        # Clean the current session as the game is finished.
        del request.session['game']

        return render(request, self.template_name, response)

    def _render_next_question(self, request):
        """
        Generate the next question's data and render the response.
        :param request: WSGIRequest object
        :return: HttpResponse
        """

        question = self.__get_current_question(request)
        response = {
            'question': question,
            'answers': question.questionanswer_set.all(),
        }

        return render(request, self.template_name, response)

    def _validate_session(self, request):
        """
        Checks if the current game session exists.
        :param request: WSGIRequest object
        :return: None
        """

        if 'game' not in request.session:
            raise Exception('The current session had not been found, please restart the game.')

    def __get_current_question(self, request):
        """
        Try to get current question from the session, otherwise raise an exception.
        :param request: WSGIRequest object
        :return: Question object or Exception
        """

        try:
            return Question.objects.get(id=request.session['game']['current_question'])
        except Question.DoesNotExist:
            raise Exception('The current question had not been found, please restart the game or contact us.')
