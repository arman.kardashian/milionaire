from django.apps import AppConfig


class GameComponentsConfig(AppConfig):
    name = 'game_components'
