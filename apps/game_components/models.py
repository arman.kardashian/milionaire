
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class Question(models.Model):

    description = models.TextField()
    score = models.IntegerField(validators=[MinValueValidator(5), MaxValueValidator(20)])

    def __str__(self):
        return 'Question ID: {}'.format(self.pk)


class QuestionAnswer(models.Model):

    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    is_right = models.BooleanField(default=False)

    def __str__(self):
        return 'QuestionAnswer ID: {}'.format(self.pk)
