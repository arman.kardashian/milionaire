=================
Who wants to be a millionaire
=================

-----------------
Development setup
-----------------

Install required system packages:

.. code-block:: bash

    $ sudo apt-get install python3-pip
    $ sudo apt-get install python-dev

Install requirements for a project.

.. code-block:: bash

    $ cd /var/www/milion && pip install -r requirements.txt
